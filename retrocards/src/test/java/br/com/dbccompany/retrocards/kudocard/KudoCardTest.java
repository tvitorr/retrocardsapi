package br.com.dbccompany.retrocards.kudocard;

import br.com.dbccompany.retrocards.entity.KudoBox;
import br.com.dbccompany.retrocards.entity.KudoCard;
import br.com.dbccompany.retrocards.entity.Membro;
import br.com.dbccompany.retrocards.entity.Sprint;
import br.com.dbccompany.retrocards.enuns.TipoStatus;
import br.com.dbccompany.retrocards.exceptions.MsgException;
import br.com.dbccompany.retrocards.service.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class KudoCardTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    ApplicationContext context;

    @MockBean
    private KudoCardService kudoCardService;

    @Test
    public void naoSalvarKudoCardSemTitulo() throws ParseException {
        KudoCardService kudoCardService = new KudoCardService();

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date data = formato.parse("23/11/2015");

        Membro membro = new Membro();
        membro.setFacilitador(true);
        membro.setEmail("maria@gmail.com");
        membro.setLogin("ma2016");
        membro.setSenha("12345");
        entityManager.persist(membro);

        Sprint sprint = new Sprint();
        sprint.setDataConclusao(data);
        sprint.setDataInicio(data);
        sprint.pushMembros(membro);
        sprint.setTitulo("Sprint linda");



        KudoBox kudoBox = new KudoBox();
        kudoBox.setTitulo("titulo do kudo box");
        kudoBox.setStatus(TipoStatus.CONCLUIDA);
        kudoBox.setDataAbertura(data);
        kudoBox.setSprint(sprint);
        entityManager.persist(kudoBox);

        sprint.pushKudoBox(kudoBox);


        KudoCard kudoCard = new KudoCard();
        kudoCard.setMembro(membro);
        kudoCard.setTitulo(null);
        kudoCard.setDataDeCriacao(data);
        kudoCard.setDescricao("Você foi incrível neste sprint");
        kudoCard.setRemetente("Mazia");
        kudoCard.setDestinatario("João");
        kudoCard.setKudoBox(kudoBox);

        kudoBox.pushKudoCrad(kudoCard);
        entityManager.persist(kudoCard);

        membro.pushSprints(sprint);
        entityManager.persist(sprint);

        entityManager.flush();

        try {
            kudoCardService.salvar(kudoCard);
        } catch (Exception e) {
            Assert.assertEquals(MsgException.EH_PRECISO_INFORMAR_TITULO, e.getMessage());
        }

    }

    @Test
    public void naoDeletarSeStatusKudoBoxForDiferenteAndamento() throws Exception {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date data = formato.parse("23/11/2015");

        Membro membro = new Membro();
        membro.setFacilitador(true);
        membro.setEmail("maria@gmail.com");
        membro.setLogin("ma2016");
        membro.setSenha("12345");
        entityManager.persist(membro);

        Sprint sprint = new Sprint();
        sprint.setDataConclusao(data);
        sprint.setDataInicio(data);
        sprint.pushMembros(membro);
        sprint.setTitulo("Sprint linda");

        KudoBox kudoBox = new KudoBox();
        kudoBox.setTitulo("titulo do kudo box");
        kudoBox.setStatus(TipoStatus.CONCLUIDA); //não é possível
        kudoBox.setDataAbertura(data);
        kudoBox.setSprint(sprint);
        entityManager.persist(kudoBox);

        sprint.pushKudoBox(kudoBox);

        KudoCard kudoCard = new KudoCard();
        kudoCard.setMembro(membro);
        kudoCard.setTitulo("Incrível!!!");
        kudoCard.setDataDeCriacao(data);
        kudoCard.setDescricao("Você foi incrível neste sprint");
        kudoCard.setRemetente("Mazia");
        kudoCard.setDestinatario("João");
        kudoCard.setKudoBox(kudoBox);

        kudoBox.pushKudoCrad(kudoCard);
        entityManager.persist(kudoCard);

        membro.pushSprints(sprint);
        entityManager.persist(sprint);

        entityManager.flush();

        entityManager.refresh(membro);

        doThrow(new Exception(MsgException.NAO_PODE_SER_DELETADO)).when(kudoCardService).deletar(1l);

    }
}