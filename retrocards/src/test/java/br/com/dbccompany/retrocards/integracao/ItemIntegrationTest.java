package br.com.dbccompany.retrocards.integracao;

import br.com.dbccompany.retrocards.entity.Membro;
import br.com.dbccompany.retrocards.entity.Sprint;
import br.com.dbccompany.retrocards.enuns.TipoItem;
import br.com.dbccompany.retrocards.enuns.TipoStatus;
import br.com.dbccompany.retrocards.entity.Item;
import br.com.dbccompany.retrocards.entity.Retrospectiva;
import br.com.dbccompany.retrocards.service.MembroService;
import br.com.dbccompany.retrocards.service.ItemService;
import br.com.dbccompany.retrocards.service.RetrospectivaService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;


@RunWith( SpringRunner.class )
public class ItemIntegrationTest {



    @Test
    public void tituloNaoPodeSerVazio() throws Exception {

        ItemService itemService = new ItemService();
        RetrospectivaService retrospectivaService = new RetrospectivaService();
        MembroService membroService = new MembroService();


        Membro membro = new Membro();
        membro.setFacilitador(true);
        membro.setSenha("112");
        membroService.salvar(membro);

        Sprint spritn = new Sprint();

        Retrospectiva retro = new Retrospectiva();
        retro.setStatus(TipoStatus.EM_ANDAMENTO);
        retrospectivaService.salvar(retro);

        Item item = new Item();
        item.setDescricao("Descricao de teste");
        item.setTipo(TipoItem.O_QUE_FUNCIONOU_BEM);
        item.setRetrospectiva(retro);

        try {
            itemService.salvar(item);
        }catch (Exception e){
            Assert.assertEquals("É preciso informar um titulo",e.getMessage());
        }
    }

    @Test
    public void descricaoNaoPodeSerVazio(){
        ItemService itemService = new ItemService();

        Item item = new Item();
        item.setTitulo("Titulo de teste");
        item.setTipo(TipoItem.O_QUE_FUNCIONOU_BEM);

        try {
            itemService.salvar(item);
        }catch (Exception e){
            Assert.assertEquals("É preciso informar uma descrição",e.getMessage());
        }
    }

    @Test
    public void tipoNaoPodeSerVazio(){
        ItemService itemService = new ItemService();

        Item item = new Item();
        item.setTitulo("Titulo de teste");
        item.setDescricao("Descricao de teste");

        try{
            itemService.salvar(item);
        }

        catch (Exception e) {
            Assert.assertEquals("É preciso ter um tipo",e.getMessage());
        }
    }

}

