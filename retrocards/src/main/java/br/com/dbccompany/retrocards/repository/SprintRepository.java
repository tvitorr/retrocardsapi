package br.com.dbccompany.retrocards.repository;

import br.com.dbccompany.retrocards.entity.Sprint;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SprintRepository extends CrudRepository<Sprint, Long> {
}
