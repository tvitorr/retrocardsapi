package br.com.dbccompany.retrocards.service;

import br.com.dbccompany.retrocards.entity.Item;
import br.com.dbccompany.retrocards.entity.Retrospectiva;
import br.com.dbccompany.retrocards.enuns.TipoItem;
import br.com.dbccompany.retrocards.enuns.TipoStatus;
import br.com.dbccompany.retrocards.exceptions.MsgException;
import br.com.dbccompany.retrocards.repository.ItemRepository;
import br.com.dbccompany.retrocards.repository.RetrospectivaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private RetrospectivaRepository retrospectivaRepository;

    @Transactional(rollbackFor = Exception.class)
    public Item salvar(Item item) throws Exception {
        Retrospectiva retrospectiva = retrospectivaRepository.findById(item.getRetrospectiva().getId()).get();
        item.setRetrospectiva(retrospectiva);

        if (item.getDescricao() == null) {
            throw new Exception(MsgException.EH_PRECISO_INFORMAR_DESCRICAO);
        }
        if (item.getTitulo() == null) {
            throw new Exception(MsgException.EH_PRECISO_INFORMAR_TITULO);
        }
        if (item.getTipo() == null) {
            throw new Exception(MsgException.EH_PRECISO_INFORMAR_TIPO);
        }

        if (item.getRetrospectiva().getStatus() != TipoStatus.EM_ANDAMENTO) {
            throw new Exception(MsgException.ITEM_NAO_PODE_SER_SALVO);
        }
        return itemRepository.save(item);
    }

    @Transactional(rollbackFor = Exception.class)
    public Item editar(long id, Item cliente) {
        cliente.setId(id);
        return itemRepository.save(cliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteById(Long id) throws Exception {
        if (itemRepository.findById(id).get().getRetrospectiva().getStatus() == TipoStatus.CONCLUIDA) {
            throw new Exception(MsgException.ITEM_NAO_PODE_SER_DELETADO);
        }
        itemRepository.deleteById(id);
    }

    public List<Item> buscarTodos() {
        return itemRepository.findAll();
    }

    public Optional<Item> buscarPorId(Long id) {
        return itemRepository.findById(id);
    }

    public List<Item> buscarPorTipo(TipoItem tipoItem) {
        return itemRepository.findByTipo(tipoItem);
    }

    public List<Item> buscarPorRetrosPectiva(Long idRetrospectiva) {
        Optional<Retrospectiva> retrospectiva = retrospectivaRepository.findById(idRetrospectiva);
        return itemRepository.findByRetrospectiva(retrospectiva);
    }
}