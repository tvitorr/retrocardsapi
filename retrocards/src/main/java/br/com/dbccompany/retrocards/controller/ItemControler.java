package br.com.dbccompany.retrocards.controller;

import br.com.dbccompany.retrocards.entity.Item;
import br.com.dbccompany.retrocards.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/item")
public class ItemControler {

    @Autowired
    ItemService itemService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Item> BuscarItens() {
        return itemService.buscarTodos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Item novoItem(@RequestBody Item item) throws Exception {
        return itemService.salvar(item);
    }

    @DeleteMapping(value = "/deletar/{id}")
    public ResponseEntity<Item> deleteById(@PathVariable Long id) throws Exception {
        itemService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public Optional<Item> ItemComId(@PathVariable long id) {
        return itemService.buscarPorId(id);
    }

}