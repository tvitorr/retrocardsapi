package br.com.dbccompany.retrocards.exceptions;

public class MsgException {

    //geral
    public static final String EH_PRECISO_INFORMAR_DESCRICAO = "É preciso informar uma descrição";
    public static final String EH_PRECISO_INFORMAR_TITULO = "É preciso informar um titulo";
    public static final String EH_PRECISO_INFORMAR_TIPO = "É preciso ter um tipo";
    public static final String USUARIO_INVALIDO = "usuario inválido";
    public static final String VOCE_NAO_EH_FACILITADOR = "Você não é facilitador!";
    public static final String DATA_INVALIDA = "A data atual é maior que a data de abertura.";

    //kudocard
    public static final String EH_PRECISO_INFORMAR_DESTINATARIO = "É preciso ter um destinatario";
    public static final String VOCE_NAO_PODE_DELETAR = "Não é possível deletar este Kudo Card, você não é o dono dele!";
    public static final String NAO_PODE_SER_DELETADO = "Kudo Box não esta em ANDAMENTO!";

    //Item
    public static final String ITEM_NAO_PODE_SER_SALVO = "Item não pode ser salvo! Verifique se a Retorspectiva está em andamento.";
    public static final String ITEM_NAO_PODE_SER_DELETADO = "Item não pode ser deletado! Verifique se a Retorspectiva está em andamento.";

    //Retrospectiva
    public static final String RETORSPECTIVA_NAO_PODE_SER_CRIADA = "É necessário que não haja nenhuma retrospectiva em andamento para que seja criada uma nova";

    //Kudobox
    public static final String USUARIO_SEM_PERMISSAO_NESTA_SPRINT = "Não possui permissão para criar Kudo Box desta Sprint";
}
