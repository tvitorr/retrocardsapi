package br.com.dbccompany.retrocards.service;

import br.com.dbccompany.retrocards.entity.KudoCard;
import br.com.dbccompany.retrocards.entity.Membro;
import br.com.dbccompany.retrocards.enuns.TipoStatus;
import br.com.dbccompany.retrocards.exceptions.MsgException;
import br.com.dbccompany.retrocards.repository.KudoBoxRepository;
import br.com.dbccompany.retrocards.repository.KudoCardRepository;
import br.com.dbccompany.retrocards.repository.MembroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class KudoCardService {

    @Autowired
    private KudoCardRepository kudoCardRepository;

    @Autowired
    private KudoBoxRepository kudoBoxRepository;

    @Autowired
    private MembroRepository membroRepository;

    @Transactional(rollbackFor = Exception.class)
    public KudoCard salvar(KudoCard kudoCard) throws Exception {
        if(kudoCard.getTitulo().equals(null)){
            throw new Exception(MsgException.EH_PRECISO_INFORMAR_TITULO);
        }
        if(kudoCard.getDescricao() == null){
            throw  new Exception(MsgException.EH_PRECISO_INFORMAR_DESCRICAO);
        }
        if(kudoCard.getDestinatario() == null){
            throw  new Exception(MsgException.EH_PRECISO_INFORMAR_DESTINATARIO);
        }
        return kudoCardRepository.save(kudoCard);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Long idKudoCard) throws Exception {
        KudoCard kudoCard = kudoCardRepository.findById(idKudoCard).get();
        Authentication aut = SecurityContextHolder.getContext().getAuthentication();
        String nomeUsuarioLogado = aut.getName();
        Membro membroChamada = membroRepository.findByLogin(nomeUsuarioLogado);

        if(kudoCard.getMembro().getId() == membroChamada.getId()){
            if(kudoBoxRepository.findById(idKudoCard).get().getStatus().equals(TipoStatus.EM_ANDAMENTO))
                kudoCardRepository.deleteById(idKudoCard);
            else
                throw new Exception(MsgException.NAO_PODE_SER_DELETADO);
        }else {
            throw new Exception(MsgException.VOCE_NAO_PODE_DELETAR);
        }
    }

    public List<KudoCard> buscarTodos(){
        return (List<KudoCard>) kudoCardRepository.findAll();
    }

    public KudoCard detalhesDoKudoCard(Long id){
            return kudoCardRepository.findById(id).get();
    }

    public KudoCard buscarPorId(Long id){
        return kudoCardRepository.findById(id).orElse(null);
    }
}