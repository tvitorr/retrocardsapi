package br.com.dbccompany.retrocards.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "MEMBRO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Membro.class)
public class Membro implements Serializable {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "MEMBRO_SEQ", sequenceName = "MEMBRO_SEQ")
    @GeneratedValue(generator = "MEMBRO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "LOGIN", nullable = false, length = 50)
    private String login;

    @Column(name = "SENHA", nullable = false)
    private String senha;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    private boolean facilitador = false;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "MEMBRO_SPRINT",
            joinColumns = {
                    @JoinColumn(name = "ID_MEMBRO")},
            inverseJoinColumns = {
                    @JoinColumn(name = "ID_SPRINT")})
    private List<Sprint> sprints = new ArrayList<>();


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getFacilitador() {
        return facilitador;
    }

    public void setFacilitador(boolean facilitador) {
        this.facilitador = facilitador;
    }

    public List<Sprint> getSprints() {
        return sprints;
    }

    public void setSprints(List<Sprint> sprints) {
        this.sprints = sprints;
    }

    public void pushSprints(Sprint... sprints) {
        this.sprints.addAll(Arrays.asList(sprints));
    }
}