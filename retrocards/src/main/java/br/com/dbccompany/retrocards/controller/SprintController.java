package br.com.dbccompany.retrocards.controller;

import br.com.dbccompany.retrocards.entity.Sprint;
import br.com.dbccompany.retrocards.service.MembroService;
import br.com.dbccompany.retrocards.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/sprint")
public class SprintController {

    @Autowired
    SprintService sprintService;

    @Autowired
    MembroService membroService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Sprint> buscarSprints() {
        return sprintService.buscarTodos();
    }

    @PostMapping(value = "/novo/")
    @ResponseBody
    public Sprint novoSprint(@RequestBody Sprint sprint) throws Exception {
        return sprintService.salvar(sprint);
    }
}