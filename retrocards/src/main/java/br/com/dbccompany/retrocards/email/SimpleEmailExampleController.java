package br.com.dbccompany.retrocards.email;

import br.com.dbccompany.retrocards.entity.Item;
import br.com.dbccompany.retrocards.enuns.TipoItem;
import br.com.dbccompany.retrocards.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class SimpleEmailExampleController {

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    ItemService itemService;

    @ResponseBody
    @RequestMapping("/enviaremailsimples")
    public String sendSimpleEmail() {

        // Create a Simple MailMessage.
        SimpleMailMessage message = new SimpleMailMessage();

        String mensagem = geraListaDoTipo(TipoItem.O_QUE_FUNCIONOU_BEM);

        message.setTo(MyConstants.FRIEND_EMAIL);
        message.setSubject("Retrospectiva tal");
        message.setText(mensagem);

        // Send Message!
        this.emailSender.send(message);

        return "Email Sent!";
    }


    public String geraListaDoTipo(TipoItem tipoItem){
        List<Item>  itens =itemService.buscarPorTipo(tipoItem);
        StringBuffer mensagem = new StringBuffer();

        for (Item item: itens) {
            mensagem.append(item.getDescricao());
        }

        return mensagem.toString();

    }


}
