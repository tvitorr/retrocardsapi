package br.com.dbccompany.retrocards.controller;

import br.com.dbccompany.retrocards.entity.Retrospectiva;
import br.com.dbccompany.retrocards.service.RetrospectivaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/retrospectiva")
public class RetrospectivaController {

    @Autowired
    private RetrospectivaService retrospectivaService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Retrospectiva> listarRetrospectivas(){
        return retrospectivaService.buscarTodos();
    }

    @PostMapping(value = "/novo/")
    @ResponseBody
    public Retrospectiva novoRetrospectiva(@RequestBody Retrospectiva retrospectiva) {
        try {
            return retrospectivaService.salvar(retrospectiva);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping(value="/concluir/{id}")
    @ResponseBody
    public Retrospectiva retrospectivaConcluida(@PathVariable Long id){
    return retrospectivaService.editar(id);
    }
}