package br.com.dbccompany.retrocards.service;

import br.com.dbccompany.retrocards.entity.Membro;
import br.com.dbccompany.retrocards.entity.Retrospectiva;
import br.com.dbccompany.retrocards.enuns.TipoStatus;
import br.com.dbccompany.retrocards.exceptions.MsgException;
import br.com.dbccompany.retrocards.repository.MembroRepository;
import br.com.dbccompany.retrocards.repository.RetrospectivaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RetrospectivaService {

    @Autowired
    private RetrospectivaRepository retrospectivaRepository;

    @Autowired
    private MembroRepository membroRepository;

    @Transactional(rollbackFor = Exception.class)
    public Retrospectiva salvar(Retrospectiva retrospectiva) throws Exception {

        Authentication aut = SecurityContextHolder.getContext().getAuthentication();
        String nomeUsuarioLogado = aut.getName();
        Membro membroChamada = membroRepository.findByLogin(nomeUsuarioLogado);

        List<Retrospectiva> retrospectivas = buscarTodos();
        boolean verificadorExisteRetroEmAndamento = false;

        for (Retrospectiva existentes : retrospectivas) {
            if (existentes.getStatus().equals(TipoStatus.EM_ANDAMENTO)) verificadorExisteRetroEmAndamento = true;
        }

        if (membroChamada.getFacilitador() && !verificadorExisteRetroEmAndamento) {
            retrospectiva.setStatus(TipoStatus.EM_ANDAMENTO);
            return retrospectivaRepository.save(retrospectiva);
        } else
            throw new Exception(MsgException.RETORSPECTIVA_NAO_PODE_SER_CRIADA);
    }

    @Transactional(rollbackFor = Exception.class)
    public Retrospectiva editar(Long id) {
        Retrospectiva retro = retrospectivaRepository.findById(id).get();
        retro.setStatus(TipoStatus.CONCLUIDA);
        return retrospectivaRepository.save(retro);
    }

    public List<Retrospectiva> buscarTodos() {
        return (List<Retrospectiva>) retrospectivaRepository.findAll();
    }
}