package br.com.dbccompany.retrocards.service;

import br.com.dbccompany.retrocards.repository.MembroRepository;
import br.com.dbccompany.retrocards.entity.Membro;
import br.com.dbccompany.retrocards.entity.Sprint;
import br.com.dbccompany.retrocards.repository.SprintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class MembroService {

    @Autowired
    private MembroRepository membroRepository;

    @Autowired
    private SprintRepository sprintRepository;

    @Transactional(rollbackFor = Exception.class)
    public Membro salvar(Membro membro) {
        List<Sprint> sprintExistetes = new ArrayList<>();
        if (membro.getSprints().size() > 0) {
            for (Sprint sprint : membro.getSprints()) {
                sprintExistetes.add(sprintRepository.findById(sprint.getId()).get());
            }
            membro.setSprints(sprintExistetes);
        }

        String senhaDescrip = membro.getSenha();
        membro.setSenha(new BCryptPasswordEncoder(6).encode(senhaDescrip));
        return membroRepository.save(membro);
    }

    @Transactional(rollbackFor = Exception.class)
    public Membro editarMembro(long id, Membro membro) {
        membro.setId(id);
        return membroRepository.save(membro);
    }

    public Membro buscarMembro(long id) {
        return membroRepository.findById(id).get();
    }

    public List<Membro> allMembros() {
        return (List<Membro>) membroRepository.findAll();
    }
}