package br.com.dbccompany.retrocards.repository;

import br.com.dbccompany.retrocards.entity.KudoBox;
import br.com.dbccompany.retrocards.enuns.TipoStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KudoBoxRepository extends CrudRepository<KudoBox, Long> {

    KudoBox findByStatus(TipoStatus status);
}