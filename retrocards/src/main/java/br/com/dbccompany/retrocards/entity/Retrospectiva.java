package br.com.dbccompany.retrocards.entity;

import br.com.dbccompany.retrocards.enuns.TipoStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "RETROSPECTIVA")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Retrospectiva.class)
public class Retrospectiva {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "RETROSPECTIVA_SEQ", sequenceName = "RETROSPECTIVA_SEQ")
    @GeneratedValue(generator = "RETROSPECTIVA_SEQ", strategy = GenerationType.SEQUENCE)
    private long Id;

    @Column(name = "TITULO", nullable = false, length = 150)
    private String titulo;

    @Column(name = "DATA_REUNIAO", nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataReuniao;


    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false)
    private TipoStatus status;

    @Column(name = "QTD_ITENS")
    private Integer qtdItens;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_SPRINT")
    private Sprint sprint;

    @OneToMany(mappedBy = "retrospectiva")
    @JsonIgnore
    private List<Item> itens = new ArrayList<>();

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getDataReuniao() {
        return dataReuniao;
    }

    public void setDataReuniao(Date dataReuniao) {
        this.dataReuniao = dataReuniao;
    }

    public TipoStatus getStatus() {
        return status;
    }

    public void setStatus(TipoStatus status) {
        this.status = status;
    }

    public Integer getQtdItens() {
        return qtdItens;
    }

    public void setQtdItens(Integer qtdItens) {
        this.qtdItens = qtdItens;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }
}