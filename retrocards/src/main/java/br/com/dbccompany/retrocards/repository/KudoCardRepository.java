package br.com.dbccompany.retrocards.repository;

import br.com.dbccompany.retrocards.entity.KudoCard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KudoCardRepository extends CrudRepository<KudoCard, Long> { }
