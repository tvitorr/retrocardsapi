package br.com.dbccompany.retrocards.repository;

import br.com.dbccompany.retrocards.entity.Membro;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MembroRepository extends CrudRepository<Membro, Long> {
    Membro findByLogin(String login);
}
