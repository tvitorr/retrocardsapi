package br.com.dbccompany.retrocards.controller;

import br.com.dbccompany.retrocards.entity.KudoCard;
import br.com.dbccompany.retrocards.repository.MembroRepository;
import br.com.dbccompany.retrocards.service.KudoBoxService;
import br.com.dbccompany.retrocards.service.KudoCardService;
import br.com.dbccompany.retrocards.service.MembroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/api/kudocard")
public class KudoCardController {

    @Autowired
    public KudoCardService kudoCardService;

    @Autowired
    public MembroRepository membroRepository;

    @GetMapping(value = "/")
    @ResponseBody
    public List<KudoCard> buscarKudoCards() {
        return kudoCardService.buscarTodos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public KudoCard novoKudoCard(@RequestBody KudoCard kudoCard) throws Exception {
        return kudoCardService.salvar(kudoCard);
    }

    @DeleteMapping(value = "/deletar/{id}")
    public ResponseEntity<KudoCard> deleteById(@PathVariable Long id) throws Exception {

        Authentication aut = SecurityContextHolder.getContext().getAuthentication();
        String logado = aut.getName();

        if (logado.equals(kudoCardService.buscarPorId(id).getMembro().getLogin())) {
            throw new Exception("token invalido");

        }
        kudoCardService.deletar(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/detalhes/{id}")
    @ResponseBody
    public KudoCard detalhesDoKudoCard(@PathVariable Long id) {
        return kudoCardService.detalhesDoKudoCard(id);
    }

}