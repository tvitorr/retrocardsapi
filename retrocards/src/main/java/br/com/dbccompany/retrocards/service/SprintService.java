package br.com.dbccompany.retrocards.service;

import br.com.dbccompany.retrocards.entity.Membro;
import br.com.dbccompany.retrocards.entity.Sprint;
import br.com.dbccompany.retrocards.exceptions.MsgException;
import br.com.dbccompany.retrocards.repository.MembroRepository;
import br.com.dbccompany.retrocards.repository.SprintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SprintService {

    @Autowired
    private SprintRepository sprintRepository;

    @Autowired
    private MembroRepository membroRepository;

    @Transactional(rollbackFor = Exception.class)
    public Sprint salvar(Sprint sprint) throws Exception {

        Authentication aut = SecurityContextHolder.getContext().getAuthentication();
        String nomeUsuarioLogado = aut.getName();
        if (nomeUsuarioLogado == null)
            throw new Exception(MsgException.USUARIO_INVALIDO);

        Membro membroChamada = membroRepository.findByLogin(nomeUsuarioLogado);
        if (membroChamada.getFacilitador()) {
            //Todos os membros vão receber a sprint
            List<Membro> membros = (List<Membro>) membroRepository.findAll();
            for (Membro membro : membros) {
                sprint.pushMembros(membro);
            }

            return sprintRepository.save(sprint);
        }
        throw new Exception(MsgException.VOCE_NAO_EH_FACILITADOR);
    }

    public List<Sprint> buscarTodos() {
        return (List<Sprint>) sprintRepository.findAll();
    }

    public Sprint buscarPorId(long id) {
        return sprintRepository.findById(id).get();
    }
}