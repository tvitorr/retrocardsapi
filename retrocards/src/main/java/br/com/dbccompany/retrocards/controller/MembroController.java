package br.com.dbccompany.retrocards.controller;


import br.com.dbccompany.retrocards.entity.Membro;
import br.com.dbccompany.retrocards.service.MembroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/membro")
public class MembroController {
    @Autowired
    MembroService membroService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Membro> lstMembros() {
        return membroService.allMembros();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Membro novoMembro(@RequestBody Membro membro) {
        return membroService.salvar(membro);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Membro editarMembro(@PathVariable long id, @RequestBody Membro membro) {
        return membroService.editarMembro(id, membro);
    }
}