package br.com.dbccompany.retrocards.repository;

import br.com.dbccompany.retrocards.entity.Retrospectiva;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RetrospectivaRepository extends CrudRepository<Retrospectiva, Long> { }
