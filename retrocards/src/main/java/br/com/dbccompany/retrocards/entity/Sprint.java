package br.com.dbccompany.retrocards.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
public class Sprint {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "SPRINT_SEQ", sequenceName = "SPRINT_SEQ")
    @GeneratedValue(generator = "SPRINT_SEQ", strategy = GenerationType.SEQUENCE)
    private long Id;

    @Column(name = "TITULO", nullable = false, length = 150)
    private String titulo;

    @Column(name = "DATA_INICIO", nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataInicio;

    @Column(name = "DATA_CONCLUSAO", nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataConclusao;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "MEMBRO_SPRINT",
            joinColumns = {
                    @JoinColumn(name = "ID_SPRINT")},
            inverseJoinColumns = {
                    @JoinColumn(name = "ID_MEMBRO")})
    @JsonIgnore
    private List<Membro> membros = new ArrayList<>();

    @OneToMany(mappedBy = "sprint", cascade = CascadeType.MERGE)
    @JsonIgnore
    private List<KudoBox> kudoBoxs = new ArrayList<>();

    @OneToMany(mappedBy = "sprint")
    @JsonIgnore
    private List<Retrospectiva> retrospectivas = new ArrayList<>();

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(Date dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public List<Membro> getMembros() {
        return membros;
    }

    public void pushMembros(Membro... membros) {
        this.membros.addAll(Arrays.asList(membros));
    }

    public List<KudoBox> getKudoBoxs() {
        return kudoBoxs;
    }

    public void pushKudoBox(KudoBox... kudoBox) {
        this.kudoBoxs.addAll(Arrays.asList(kudoBox));
    }

    public List<Retrospectiva> getRetrospectivas() {
        return retrospectivas;
    }

    public void pushRetrospectivas(Retrospectiva... retrospectivas) {
        this.retrospectivas.addAll(Arrays.asList(retrospectivas));
    }
}