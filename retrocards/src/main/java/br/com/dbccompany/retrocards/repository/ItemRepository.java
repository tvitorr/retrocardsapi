package br.com.dbccompany.retrocards.repository;

import br.com.dbccompany.retrocards.entity.Item;
import br.com.dbccompany.retrocards.entity.Retrospectiva;
import br.com.dbccompany.retrocards.enuns.TipoItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
    List<Item> findAll();

    List<Item> findByTipo(TipoItem tipoItem);

    List<Item> findByRetrospectiva(Optional<Retrospectiva> retrospectiva);
}
