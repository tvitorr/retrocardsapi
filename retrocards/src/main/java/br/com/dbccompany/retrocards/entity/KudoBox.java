package br.com.dbccompany.retrocards.entity;

import br.com.dbccompany.retrocards.enuns.TipoStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "KUDO_BOX")
public class KudoBox {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "KUDO_BOX_SEQ", sequenceName = "KUDO_BOX_SEQ")
    @GeneratedValue(generator = "KUDO_BOX_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "TITULO", nullable = false, length = 150)
    private String titulo;

    @Column(name = "DATA_ABERTURA", nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataAbertura;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private TipoStatus status;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_SPRINT")
    private Sprint sprint;

    @OneToMany(mappedBy = "kudoBox", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<KudoCard> kudoCards = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getDataAbertura() {
        return dataAbertura;
    }

    public void setDataAbertura(Date dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    public TipoStatus getStatus() {
        return status;
    }

    public void setStatus(TipoStatus status) {
        this.status = status;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public List<KudoCard> getKudoCards() {
        return kudoCards;
    }

    public void pushKudoCrad(KudoCard... kudoCard) {
        this.kudoCards.addAll(Arrays.asList(kudoCard));
    }
}