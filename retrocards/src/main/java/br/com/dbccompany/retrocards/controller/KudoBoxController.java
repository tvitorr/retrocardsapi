package br.com.dbccompany.retrocards.controller;

import br.com.dbccompany.retrocards.entity.KudoBox;
import br.com.dbccompany.retrocards.enuns.TipoStatus;
import br.com.dbccompany.retrocards.service.KudoBoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/kudobox")
public class KudoBoxController {

    @Autowired
    public KudoBoxService kudoBoxService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<KudoBox> buscarKudoBox() {
        return kudoBoxService.buscarTodos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public KudoBox novoKudoBox(@RequestBody KudoBox kudoBox) throws Exception {
        return kudoBoxService.salvar(kudoBox);
    }

    @DeleteMapping(value = "/deletar/{id}")
    public ResponseEntity<KudoBox> deletar(@PathVariable Long id) throws Exception {
        kudoBoxService.deletar(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/status")
    @ResponseBody
    public KudoBox buscarKudoBoxPorStatus(@RequestBody TipoStatus status) {
        return kudoBoxService.buscarPorStatus(status);
    }
}