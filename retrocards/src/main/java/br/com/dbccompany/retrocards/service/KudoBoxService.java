package br.com.dbccompany.retrocards.service;

import br.com.dbccompany.retrocards.entity.KudoBox;
import br.com.dbccompany.retrocards.entity.Membro;
import br.com.dbccompany.retrocards.entity.Sprint;
import br.com.dbccompany.retrocards.enuns.TipoStatus;
import br.com.dbccompany.retrocards.exceptions.MsgException;
import br.com.dbccompany.retrocards.repository.KudoBoxRepository;
import br.com.dbccompany.retrocards.repository.MembroRepository;
import br.com.dbccompany.retrocards.repository.SprintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
public class KudoBoxService {

    @Autowired
    private KudoBoxRepository kudoBoxRepository;

    @Autowired
    private SprintRepository sprintRepository;

    @Autowired
    private MembroRepository membroRepository;

    @Transactional(rollbackFor = Exception.class)
    public KudoBox salvar(KudoBox kudoBox) throws Exception {
        Authentication aut = SecurityContextHolder.getContext().getAuthentication();
        String nomeUsuarioLogado = aut.getName();
        if (nomeUsuarioLogado == null)
            throw new Exception(MsgException.USUARIO_INVALIDO);
        Membro membroChamada = membroRepository.findByLogin(nomeUsuarioLogado);
        if (membroChamada.getFacilitador()) {
            if (!isDataAberturaMaiorAtual(kudoBox.getDataAbertura()))

                throw new Exception(MsgException.DATA_INVALIDA);

            if (temUmaKudoBoxAberta(kudoBox.getSprint().getId())) {
                kudoBox.setStatus(TipoStatus.CRIADA);
                return kudoBoxRepository.save(kudoBox);
            } else {
                kudoBox.setStatus(TipoStatus.EM_ANDAMENTO);
                return kudoBoxRepository.save(kudoBox);
            }

        } else {
            throw new Exception(MsgException.USUARIO_SEM_PERMISSAO_NESTA_SPRINT);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Long id) {
        kudoBoxRepository.deleteById(id);
    }

    public List<KudoBox> buscarTodos() {
        return (List<KudoBox>) kudoBoxRepository.findAll();
    }

    public KudoBox buscarPorId(long id) {
        return kudoBoxRepository.findById(id).get();
    }

    public KudoBox buscarPorStatus(TipoStatus status) {
        return kudoBoxRepository.findByStatus(status);
    }

    private boolean isDataAberturaMaiorAtual(Date dataAbertura) {
        String zone = "America/Sao_Paulo";
        LocalDateTime localDateTime = convertParaLocalDateViaInstant(dataAbertura);
        if (localDateTime.isAfter(horaLocal(zone))) {
            return true;
        }
        return false;
    }

    private LocalDateTime horaLocal(String zone) {
        return LocalDateTime.now(ZoneId.of(zone));
    }

    public LocalDateTime convertParaLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    private Boolean temUmaKudoBoxAberta(Long idSprint) {
        Sprint sprint = sprintRepository.findById(idSprint).get();
        List<KudoBox> kudoBoxes = sprint.getKudoBoxs();
        for (KudoBox kudoBox : kudoBoxes) {
            if (kudoBox.getStatus().equals(TipoStatus.EM_ANDAMENTO))
                return true;
        }
        return false;
    }
}