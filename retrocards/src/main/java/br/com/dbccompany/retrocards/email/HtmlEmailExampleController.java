package br.com.dbccompany.retrocards.email;

import br.com.dbccompany.retrocards.service.ItemService;
import br.com.dbccompany.retrocards.entity.Item;
import br.com.dbccompany.retrocards.enuns.TipoItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Controller
public class HtmlEmailExampleController {

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    ItemService itemService;

    @ResponseBody
    @RequestMapping("/sendHtmlEmail/{id}")
    public String sendHtmlEmail(@PathVariable Long id)throws MessagingException {

        MimeMessage message = emailSender.createMimeMessage();

        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

        String funcionouBem = geraListaDoTipo(id,TipoItem.O_QUE_FUNCIONOU_BEM);
        String oQueMelhorar = geraListaDoTipo(id,TipoItem.O_QUE_PODE_SER_MELHORADO);


        String htmlMsg = "<h3>O que funcionou bem?</h3>"
                +funcionouBem
                        +"<h3>O que podemos melhorar?</h3>"
                +oQueMelhorar
                ;

        message.setContent(htmlMsg, "text/html");

        helper.setTo(MyConstants.FRIEND_EMAIL);

        helper.setSubject("Retrospectiva Sprint:"+id);


        this.emailSender.send(message);

        return "Email Sent!";
    }


    public String geraListaDoTipo(Long id, TipoItem tipoItem){
        List<Item> itens =itemService.buscarPorRetrosPectiva(id);
        StringBuffer mensagem = new StringBuffer();

        for (Item item: itens) {
            if(item.getTipo()==tipoItem) {
                mensagem.append("<p>"+item.getDescricao()+"</p></br>");
            }
        }

        return mensagem.toString();

    }


}
