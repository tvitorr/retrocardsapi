package br.com.dbccompany.retrocards.security;


import br.com.dbccompany.retrocards.entity.Membro;
import br.com.dbccompany.retrocards.repository.MembroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Service("userDetailsService")
public class UserDetailsImplementacao implements UserDetailsService {

    @Autowired
    private MembroRepository membroRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Membro membro = membroRepository.findByLogin( login );

        if( membro == null ){
            throw new UsernameNotFoundException( login + "não registrado! ");
        }

        Set<GrantedAuthority> permissoes = Stream
                .of( new SimpleGrantedAuthority( membro.getLogin() ) )
                .collect( toSet() );

        return new User( membro.getLogin(), membro.getSenha(), permissoes);
    }
}

