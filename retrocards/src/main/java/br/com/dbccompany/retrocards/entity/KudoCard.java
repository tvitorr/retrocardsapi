package br.com.dbccompany.retrocards.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "KUDO_CARD")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = KudoCard.class)
public class KudoCard {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "KUDO_CARD_SEQ", sequenceName = "KUDO_CARD_SEQ")
    @GeneratedValue(generator = "KUDO_CARD_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "TITULO", nullable = false, length = 150)
    private String titulo;

    @Column(name = "DATA_DE_CRIACAO")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataDeCriacao;

    @Column(name = "DESCRICAO", nullable = false)
    private String descricao;

    @Column(name = "DESTINATARIO", length = 100)
    private String destinatario;

    @Column(name = "REMETENTE", length = 100)
    private String remetente;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_KUDO_BOX")
    private KudoBox kudoBox;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "MEMBRO")
    private Membro membro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getDataDeCriacao() {
        return dataDeCriacao;
    }

    public void setDataDeCriacao(Date dataDeCriacao) {
        this.dataDeCriacao = dataDeCriacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getRemetente() {
        return remetente;
    }

    public void setRemetente(String remetente) {
        this.remetente = remetente;
    }

    public KudoBox getKudoBox() {
        return kudoBox;
    }

    public void setKudoBox(KudoBox kudoBox) {
        this.kudoBox = kudoBox;
    }

    public Membro getMembro() {
        return membro;
    }

    public void setMembro(Membro membro) {
        this.membro = membro;
    }
}